#include "chat.h"
#include "config.h"

static String payload = "";

void read_serial();

void setup() {
  Serial.begin(9600);
  
  slave_init(DEVICE_ID);
}

void loop() {
  read_serial();
}

void read_serial() {
  while (Serial.available()) {
    char c = Serial.read();

    if (c != '\n') {
      payload += c;
    }
    else {
      auto space_idx = payload.indexOf(' ');

      if (space_idx != -1 && isDigit(payload[0])) {
        auto device_id = atoi(payload.c_str());

        if (device_id > 127) {
          Serial.println(F("Invalid device ID: cannot be more than 127!"));
        }

        auto message = payload.substring(space_idx + 1);
        send_message(device_id, message);
      }
      else if (payload == "users") { // show all available users
        scan_net();
      }
      else if (payload == "id") { // show DEVICE_ID
        Serial.println(String("My ID: ") + DEVICE_ID);
        Serial.println();
      }
      else if (prev_sent_device_id != byte(-1)) {
        send_message(prev_sent_device_id, payload);
      }
      else {
        Serial.println(F("Invalid message!"));
      }
      
      payload = "";
    }
  }
}
