# I2C chat

You can:
* send messages to everyone (all or someone particular) in your local chat:<br>
```<device_id> <message>```;
* response someone:<br>
```<message>```;
* see all available users:<br>
```users```;
* get your id:
```id```.

---
