#pragma once

#include <Wire.h>

#include "config.h"

static byte prev_sent_device_id = -1;

void receive_event(int);

//void master_init() {
//  Serial.println(F("Initializing master..."));
//  Wire.begin();
//}

void slave_init(byte slave_id, bool broadcasting = true) {
  Serial.print(F("Initializing slave # "));
  Serial.println(slave_id);
  
  Wire.begin(slave_id);
  Wire.onReceive(receive_event);

  if (broadcasting)
    TWAR = (slave_id << 1) | 1;
}

void scan_net() {
  int n_devices = 0;

  for (byte addr = 1; addr < 127; addr++) {
    Wire.beginTransmission(addr);
    byte error = Wire.endTransmission();

    if (error == 0) {
      Serial.print(F("Device found. ID: "));
      Serial.println(addr);
      
      n_devices++;
    }
    else if (error == 4) {
      Serial.print(F("Unknown error. Device ID: "));
      Serial.println(addr);
    }
  }

  if (n_devices == 0) {
    Serial.println(F("No devices found\n"));
  }
}

byte _receive_device_id() {
  byte device_id = -1;
  
  while (Wire.available()) {
    byte digit = Wire.read();

    if (digit == ' ') break;

    digit -= '0';

    if (device_id == byte(-1)) device_id = 0;

    device_id = device_id * 10 + digit;
  }

  return device_id;
}

String _receive_message() {
  String message = "";

  while (Wire.available()) {
    message += char(Wire.read());
  }

  return message;
}

void receive_event(int n_bytes_received) {
  if (Wire.available()) {
    auto device_id = _receive_device_id();

    if (device_id != byte(-1)) {
      auto message = _receive_message();
      prev_sent_device_id = device_id;

      Serial.print(F("User # "));
      Serial.print(device_id);
      Serial.print(F(": "));
      Serial.println(message);
    }
  }
}

void send_message(byte device_id, String message) {
  prev_sent_device_id = device_id;
  
  Wire.beginTransmission(device_id);
  Wire.write((String(DEVICE_ID) + ' ' + message).c_str());
  Wire.endTransmission();

  Serial.print(F("Me: "));
  Serial.println(message);
}
